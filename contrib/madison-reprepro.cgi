#!/usr/bin/perl -w

# Copyright (C) 2009 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

use strict;

use CGI qw/:standard/;
use DB_File; # this DB module can read all keys from a database
use BerkeleyDB; # this DB module can open sub-databases

my $dbdir = "/srv/debug.debian.net/db";
my @pkgs = split / /, param('package');

print header(-type => 'text/plain', -charset => 'utf-8');

my %hash;
tie %hash, 'DB_File', "$dbdir/packages.db", 0, 0666, $DB_BTREE;
my @dbs = keys %hash;
untie %hash;

my %archive;
my ($pkglen, $verlen, $distlen) = (0, 0, 0);

foreach my $db (@dbs) {
	#print "$db\n";
	$db =~ /(.*)\|(.*)\|(.*)/;
	my ($dist, $component, $arch) = ($1, $2, $3);
	my $bdb = tie %hash, 'BerkeleyDB::Btree',
		-Filename => "$dbdir/packages.db",
		-Subname => $db,
		-Flags => DB_RDONLY
			or die $! . $BerkeleyDB::Error;
	foreach my $pkg (@pkgs) {
		my $data = $hash{"$pkg\0"};
		next unless defined $data;
		$data =~ /^Version: (.*)/m or die "no Version in $data";
		$archive{$pkg}->{"$dist/$component"}->{$1}->{$arch} = 1;
		$pkglen = length($pkg) if length($pkg) > $pkglen;
		$verlen = length($1) if length($1) > $verlen;
		$distlen = length("$dist/$component") if length("$dist/$component") > $distlen;
	}
	undef $bdb;
	untie %hash;
}

foreach my $pkg (sort keys %archive) {
	foreach my $dist (sort keys %{$archive{$pkg}}) {
		foreach my $version (sort keys %{$archive{$pkg}->{$dist}}) {
			printf " %${pkglen}s | %${verlen}s | %${distlen}s | %s\n",
				$pkg, $version, $dist,
				join (", ",
					sort keys %{$archive{$pkg}->{$dist}->{$version}}),
				"\n";
		}
	}
}
