## Since: 4
## Match: regexp:touch \/build\(-.*\)\?-stamp$
## Condition: 3 <= rulesmode
This tag means a build log has a line touching <tt>build-stamp</tt> or <tt>build-</tt>something<tt>-stamp</tt> when processing the <tt>binary-arch</tt> target.
<br>
This test has some false positives, most likely due to stdin/stderr ordering issues
(many on hurd-i386, but also a larger number of armel and some on other architectures).
Because of this it is ignored in the numbers exported to tracker.debian.org unless
it shows up on at least 4 architectures.
<br>
<tt>build-stamp</tt> files are usually used as stamp files to make sure the upstream
build process is only called once in the build target but not a second time in the binary
target of debian/rules. As the build target is run as normal user, while the binary target
is usually run under fakeroot, this ensures the package build is not tainted by any
environment variables fakeroot might set. It also ensures compilation is only done once
and not even some possible expensive up-to-date checking is done in the binary target.
<br>
Possible problems causing this warning:
<ul>
<li>build rule not depending on the (right) build-stamp file
<li>'phony' build-stamp target:<br>
A make rule is phony if it is listed in .PHONY, if it does not create a file named
after the rule or if one of its prerequisites (recursively) is phony.
<br>
A common mistake is using a <tt>patch</tt> rule as prerequisite instead of its stamp file
or using a <tt>config.status: configure</tt> rule for non-autoconf configure that is
not generating a <tt>config.status</tt> file.
<li>binary-arch depending (indirectly) on binary-indep-build:
If e.g. the autobuilders call <tt>build-arch</tt> and your build-arch</tt> generates a
<tt>build-arch-stamp</tt> file and your <tt>binary-arch</tt>
(or a target being part of <tt>binary-arch</tt>, often called <tt>install</tt>) depends
on <tt>build</tt>, which generates <tt>build-arch-stamp</tt> and <tt>build-indep-stamp</tt>
then <tt>build-indep-stamp</tt> is only generated once <tt>binary-arch</tt> is called
and this warning is triggered. (If your build-indep target is a no-op then this is not
a problem yet, but better fix it now before you add something there and forget to fix this
issue).
<li>False positives:<br> sometimes the <tt>debian/rules binary-arch</tt> is found in the
build log before output still originating from a previously executed command.
This is usually limited to one or two, very seldom three architectures.</li>
</ul>
