qa.debian.org cronjobs
======================

To run a cronjob as the qa user, commit an executable script in the
/trunk/data/cronjobs directory. To control when to run it, the script
should have a line somewhere like:

    # CRON=25 0 * * *

Without such line, the script won't be run at all. The contents of that
line will be the prefix of the resulting line in the installed crontab,
as in:

    25 0 * * * nice -15 flock -n /srv/qa.debian.org/lock/example-script /srv/qa.debian.org/data/cronjobs/example-script

The CRON= line is not parsed, just bindly used as a prefix, so you can
do stuff like:

    # CRON=25 0 * * * [ /srv/external/file -nt /srv/myjob/stamp ] &&

Errors go to cron-errors@qa.d.o by default. You can add yourself to that address
in /trunk/mail/aliases, or you can specify an address to which errors from your
script should be sent to. To do that, include a line like:

    # MAILTO+=your@address.com

This will send errors to your address *and* to cron-errors. If your script is
very verbose, or you have some other reason, you can have the errors sent only
to yourself by using MAILTO= instead of MAILTO+=.


IMPLEMENTATION:

There is a makefile in /srv/qa.debian.org/data/cronjobs, which generates
.crontab.generated in that directory. It is automatically executed every hour
(see crontab.head).

The makefile adds a 'this is generated' header, then adds 'crontab.head', and
then adds the result of /srv/qa.debian.org/bin/crontab-parser to
.crontab.generated.

 -- Christoph Berg <myon@debian.org>  Sat, 21 Feb 2009 00:25:08 +0100
