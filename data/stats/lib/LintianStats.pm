# Copyright 2004 Frank Lichtenheld

package LintianStats;

use strict;
use warnings;

use lib '/srv/qa.debian.org/data/stats/lib';
use BinSrcMap;

use Exporter;

our @ISA = qw( Exporter );
our @EXPORT = qw( get_lintian_status get_lintian_warnings get_lintian_errors );

our $LINTIAN_STATS_FILE = '/srv/qa.debian.org/data/lintian-list.txt';

my %lintian_stats;

sub init {

    open LINTIAN_STATS, "<", $LINTIAN_STATS_FILE
	or die "Couldn't open $LINTIAN_STATS_FILE: $!";

    while (<LINTIAN_STATS>) {
	chomp;

	my ($pkg, $errors, $warnings) = split /\s+/;

	my $stat_val = $errors + $warnings*0.1;

#	print STDERR "(LintianStats) $pkg\t=>\t$stat_val,\tE: $errors/W: $warnings\n";

	$lintian_stats{$pkg} = { status => $stat_val,
				 errors => $errors,
				 warnings => $warnings,
			     }
    }

    close LINTIAN_STATS or warn;
}

sub get_lintian_status {
    return $lintian_stats{$_[0]}{status} || 0;
}

sub get_lintian_errors {
    return $lintian_stats{$_[0]}{errors} || 0;
}

sub get_lintian_warnings {
    return $lintian_stats{$_[0]}{warnings} || 0;
}

1;
